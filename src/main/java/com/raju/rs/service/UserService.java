package com.raju.rs.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.entity.RUser;
import com.raju.rs.repository.UserRepository;

@Service("userService")
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Transactional(readOnly = true)
	public List<RUser> findAllUsers() {
		logger.debug("Fetching all users ...");
		// userRepository.inOnlyTest("");
		return userRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<RUser> findByUserId(Long id) {
		return userRepository.findByUserId(id);
	}

	@Transactional
	public RUser addNewUser(RUser user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		logger.info("Adding new user : {}", user);
		return userRepository.save(user);
	}

	@Transactional
	public Optional<RUser> updateUser(RUser user) {
		Optional<RUser> userOpt = userRepository.findByUserId(user.getUserId());
		if (userOpt.isPresent()) {
			RUser userFromDB = userOpt.get();
			userFromDB.setEmailId(user.getEmailId());
			userFromDB.setFirstName(user.getFirstName());
			userFromDB.setLastName(user.getLastName());
			userFromDB.setPassword(passwordEncoder.encode(user.getPassword()));
			return Optional.of(userFromDB);
		} else {
			return Optional.empty();
		}

	}

	@Transactional
	public Boolean deleteUserById(Long id) {
		Optional<RUser> userOpt = userRepository.findByUserId(id);
		if (userOpt.isPresent()) {
			RUser userFromDB = userOpt.get();
			userRepository.delete(userFromDB);
			return true;
		} else {
			return false;
		}
	}

}
