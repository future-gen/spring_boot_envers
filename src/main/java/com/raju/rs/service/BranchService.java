package com.raju.rs.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.entity.Branch;
import com.raju.rs.repository.BranchRepository;

@Service("branchService")
public class BranchService {

	private static final Logger logger = LoggerFactory.getLogger(BranchService.class);

	private final BranchRepository branchRepository;

	@Autowired
	public BranchService(BranchRepository branchRepository) {
		this.branchRepository = branchRepository;
	}

	@Transactional(readOnly = true)
	public List<Branch> findAllBranchs() {
		logger.debug("Fetching all branchs ...");
		// branchRepository.inOnlyTest("");
		return branchRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Branch> findByBranchId(Long id) {
		return branchRepository.findByBranchId(id);
	}

	@Transactional
	public Branch addNewBranch(Branch branch) {
		logger.info("Adding new branch : {}", branch);
		return branchRepository.save(branch);
	}

	@Transactional
	public Optional<Branch> updateBranch(Branch branch) {
		Optional<Branch> branchOpt = branchRepository.findByBranchId(branch.getBranchId());
		if (branchOpt.isPresent()) {
			Branch branchFromDB = branchOpt.get();
			branchFromDB.setName(branch.getName());
			branchFromDB.setInstitute(branch.getInstitute());
			return Optional.of(branchFromDB);
		} else {
			return Optional.empty();
		}

	}

	@Transactional
	public Boolean deleteBranchById(Long id) {
		Optional<Branch> branchOpt = branchRepository.findByBranchId(id);
		if (branchOpt.isPresent()) {
			Branch branchFromDB = branchOpt.get();
			branchRepository.delete(branchFromDB);
			return true;
		} else {
			return false;
		}
	}
}
