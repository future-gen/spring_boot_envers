package com.raju.rs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.raju.rs.entity.RUser;
import com.raju.rs.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(final String emailId) {
		final RUser appUser = userRepository.findByEmailId(emailId);
		if (appUser == null) {
			throw new UsernameNotFoundException(emailId);
		}
		return new AppUserPrincipal(appUser);
	}
}