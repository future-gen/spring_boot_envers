package com.raju.rs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.entity.Branch;
import com.raju.rs.entity.Institute;
import com.raju.rs.entity.RUser;
import com.raju.rs.repository.BranchRepository;
import com.raju.rs.repository.InstituteRepository;
import com.raju.rs.repository.UserRepository;
import com.raju.rs.util.TestDataHelper;

@Service("commonService")
public class CommonService {

	private static final Logger logger = LoggerFactory.getLogger(CommonService.class);

	private final InstituteRepository instituteRepository;
	private final BranchRepository branchRepository;
	private final UserRepository userRepository;

	@Autowired
	private TestDataHelper testData;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public CommonService(InstituteRepository instituteRepository, BranchRepository branchRepository,
			UserRepository userRepository) {
		this.instituteRepository = instituteRepository;
		this.branchRepository = branchRepository;
		this.userRepository = userRepository;
	}

	@Transactional
	public void addInitialTestData() {

		logger.debug("Adding test data.");
		Institute instituteFergusson = testData.getInstituteFC();

		Branch branchFcCs = testData.getBranchFcCs();

		instituteFergusson = instituteRepository.save(instituteFergusson);

		branchFcCs = branchRepository.save(branchFcCs);

		RUser userAdmin = testData.getUserAdmin();
		userAdmin.setPassword(passwordEncoder.encode(userAdmin.getPassword()));
		userRepository.save(userAdmin);

		RUser userUser = testData.getUserUser();
		userUser.setPassword(passwordEncoder.encode(userUser.getPassword()));
		userRepository.save(userUser);
	}
}
