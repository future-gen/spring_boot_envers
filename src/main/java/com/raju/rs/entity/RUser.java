package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "R_USER")
public class RUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "USER_ID")
	private Long userId;

	@Basic(optional = false)
	@Column(name = "FIRST_NAME")
	private String firstName;

	@Basic(optional = false)
	@Column(name = "LAST_NAME")
	private String lastName;

	@Basic(optional = false)
	@Column(name = "EMAIL_ID")
	private String emailId;

	@Basic(optional = false)
	@Column(name = "PASSWORD")
	private String password;

	public RUser() {
		super();
	}

	public RUser(UserBuilder userBuilder) {
		this.userId = userBuilder.userId;
		this.firstName = userBuilder.firstName;
		this.lastName = userBuilder.lastName;
		this.emailId = userBuilder.emailId;
		this.password = userBuilder.password;
	}

	public static class UserBuilder {
		private Long userId;
		private String firstName;
		private String lastName;
		private String emailId;
		private String password;

		public UserBuilder() {
		}

		public UserBuilder setUserId(Long userId) {
			this.userId = userId;
			return this;
		}

		public UserBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public UserBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public UserBuilder setEmailId(String emailId) {
			this.emailId = emailId;
			return this;
		}

		public UserBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public RUser build() {
			return new RUser(this);
		}
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", emailId=" + emailId
				+ ", password=" + password + "]";
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
