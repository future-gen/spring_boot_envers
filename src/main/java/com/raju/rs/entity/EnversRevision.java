package com.raju.rs.entity;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import com.raju.rs.listener.MyRevisionListener;

@Entity
@RevisionEntity(MyRevisionListener.class)
@Table(name = "ENVERS_REV_INFO")
public class EnversRevision extends DefaultRevisionEntity {

	private static final long serialVersionUID = 1L;

	private String userName;

	public EnversRevision() {
		super();
	}

	@Column(name = "MODIFIED_AT")
	private OffsetDateTime modifiedAt;

	public OffsetDateTime getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(OffsetDateTime modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}