package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "BRANCH", uniqueConstraints = @UniqueConstraint(columnNames = { "INSTITUTE_ID", "NAME" }))
@Audited
public class Branch implements Serializable, Comparable<Branch> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "BRANCH_ID")
	private Long branchId;

	@Basic(optional = false)
	@Column(name = "NAME", nullable = false)
	private String name;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "INSTITUTE_ID", nullable = false)
	private Institute institute;

	public Branch() {
		super();
	}

	private Branch(BranchBuilder builder) {
		this.branchId = builder.branchId;
		this.name = builder.name;
		this.institute = builder.institute;
	}

	public static class BranchBuilder {
		private Long branchId;
		private String name;
		private Institute institute;

		public BranchBuilder(Long branchId) {
			this.branchId = branchId;
		}

		public BranchBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public BranchBuilder setInstitute(Institute institute) {
			this.institute = institute;
			return this;
		}

		public Branch build() {
			return new Branch(this);
		}

	}

	@Override
	public int compareTo(Branch o) {
		return this.name.compareTo(o.name);
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

}
