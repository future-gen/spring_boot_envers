package com.raju.rs.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.raju.rs.entity.Branch;
import com.raju.rs.service.BranchService;

@RestController
public class BranchController {

	private static final Logger logger = LoggerFactory.getLogger(BranchController.class);

	private BranchService branchService;

	public BranchController(BranchService branchService) {
		this.branchService = branchService;
	}

	@GetMapping("/branch")
	public ResponseEntity<?> getAllBranchs() {
		List<Branch> branchs = branchService.findAllBranchs();
		if (!branchs.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(branchs);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@GetMapping("/branch/{id}")
	public ResponseEntity<?> getBranchById(@PathVariable Long id) {
		return branchService.findByBranchId(id).map(branch -> {
			return ResponseEntity.status(HttpStatus.OK).body(branch);
		}).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
	}

	@PostMapping("/branch")
	public ResponseEntity<Branch> addNewBranch(@Valid @RequestBody Branch branch) {
		logger.info("Creating new branch :", branch);
		return ResponseEntity.status(HttpStatus.CREATED).body(branchService.addNewBranch(branch));
	}

	@PutMapping("/branch/{id}")
	public ResponseEntity<?> updateBranch(@Valid @RequestBody Branch branch, @PathVariable Long id) {
		logger.info("Updating branch :" + branch);

		Optional<Branch> branchOpt = branchService.updateBranch(branch);
		if (branchOpt.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(branch);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@DeleteMapping("/branch/{id}")
	public ResponseEntity<?> deleteBranch(@PathVariable Long id) {

		logger.info("Deleting branch with ID {}", id);

		if (branchService.deleteBranchById(id)) {
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}
