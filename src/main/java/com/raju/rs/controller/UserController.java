package com.raju.rs.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.raju.rs.entity.RUser;
import com.raju.rs.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping(produces = "application/json")
	@RequestMapping({ "/validateLogin" })
	public Boolean validateLogin() {
		return true;
	}

	@GetMapping("/")
	public ResponseEntity<?> getAllUsers() {
		List<RUser> users = userService.findAllUsers();
		if (!users.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(users);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getUserById(@PathVariable Long id) {
		return userService.findByUserId(id).map(user -> {
			return ResponseEntity.status(HttpStatus.OK).body(user);
		}).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
	}

	@PostMapping("/")
	public ResponseEntity<RUser> addNewUser(@Valid @RequestBody RUser user) {
		logger.info("Creating new user :", user);
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.addNewUser(user));
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateUser(@Valid @RequestBody RUser user, @PathVariable Long id) {
		logger.info("Updating user :" + user);

		Optional<RUser> userOpt = userService.updateUser(user);
		if (userOpt.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(user);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id) {

		logger.info("Deleting user with ID {}", id);

		if (userService.deleteUserById(id)) {
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}
