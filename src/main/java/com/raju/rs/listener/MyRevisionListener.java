package com.raju.rs.listener;

import java.time.OffsetDateTime;

import org.hibernate.envers.RevisionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.raju.rs.config.IAuthenticationFacade;
import com.raju.rs.entity.EnversRevision;

public class MyRevisionListener implements RevisionListener {

	@Autowired
	private IAuthenticationFacade authenticationFacade;

	@Override
	public void newRevision(Object revisionEntity) {
		EnversRevision rev = (EnversRevision) revisionEntity;
		String userName = "admin@gmail.com";
		if (null != authenticationFacade.getAuthentication()) {
			userName = authenticationFacade.getAuthentication().getName();
		}
		rev.setUserName(userName);
		rev.setModifiedAt(OffsetDateTime.now());
	}

}