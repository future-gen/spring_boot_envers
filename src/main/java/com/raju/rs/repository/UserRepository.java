package com.raju.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.RUser;

public interface UserRepository extends JpaRepository<RUser, Long> {

	RUser findByEmailId(String emailId);

	Optional<RUser> findByEmailIdAndPassword(String emailId, String passowrd);

	Optional<RUser> findByUserId(Long userId);

}