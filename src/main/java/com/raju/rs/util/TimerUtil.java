package com.raju.rs.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Time util
 * 
 * @author raju
 *
 */
public class TimerUtil {
	private static final Logger logger = LoggerFactory.getLogger(TimerUtil.class);
	private long startTime = 0l;

	public TimerUtil() {
		this.startTime = System.currentTimeMillis();
	}

	public void stop(String msg) {
		long totalTime = (System.currentTimeMillis() - startTime);
		String logMessage = null;
		if (null == msg) {
			logMessage = "##End time : " + totalTime + " ms";
		} else {
			logMessage = "##End time : " + totalTime + " ms for class/method : " + msg;
		}
		logger.warn(logMessage);
	}
}