package com.raju.rs.util;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.raju.rs.entity.Branch;
import com.raju.rs.entity.Institute;
import com.raju.rs.entity.RUser;

@Component
@Scope("prototype") // Important for parallel test execution
public class TestDataHelper {

	private Institute instituteFC;
	private Branch branchFcCs;
	private RUser user, admin;

	public Institute getInstituteFC(boolean... newValue) {
		if (instituteFC == null || (newValue.length > 0 && newValue[0])) {
			instituteFC = new Institute.InstituteBuilder(null).setName("Fergsson College").build();
		}
		return instituteFC;
	}

	public Branch getBranchFcCs(boolean... newValue) {
		if (branchFcCs == null || (newValue.length > 0 && newValue[0])) {
			branchFcCs = new Branch.BranchBuilder(null).setInstitute(instituteFC).setName("Computer Science").build();
		}
		return branchFcCs;
	}

	public RUser getUserUser(boolean... newValue) {
		if (user == null || (newValue.length > 0 && newValue[0])) {
			user = new RUser.UserBuilder().setFirstName("user").setLastName("userTest").setEmailId("user@gmail.com")
					.setPassword("user").build();
		}
		return user;
	}

	public RUser getUserAdmin(boolean... newValue) {
		if (admin == null || (newValue.length > 0 && newValue[0])) {
			admin = new RUser.UserBuilder().setFirstName("admin").setLastName("adminTest").setEmailId("admin@gmail.com")
					.setPassword("admin").build();
		}
		return admin;
	}

}
